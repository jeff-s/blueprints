Just because you can, doesn't mean you should. Just because /I/ can, means I /absolutely/ should.

Do as I say, not as I do!