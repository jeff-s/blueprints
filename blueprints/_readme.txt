This is a collection of my Factorio blueprints from over the years.  Most of them are my creation; blueprints that I did not create will be clearly marked.

Unfortunately many of my blueprints were only on Pastebin and have been lost to the sands of time.